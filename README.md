
# python-demoapp

This project serves as a demo to showcase the implementation of a CI/CD pipeline.

I used a Flask application [template](https://github.com/benc-uk/python-demoapp.git) as the foundation, then implemented the CI/CD pipeline using an EC2 server, GitLab CI, and Docker Hub to provide free hosting for this project.

You can also checkout the application right [here](http://ec2-52-47-127-207.eu-west-3.compute.amazonaws.com/).

## Pipeline 🏗️

![Pipeline Screenshot](https://i.ibb.co/1KR7DS5/Screenshot-from-2024-05-20-12-50-21.png)

This GitLab pipeline is structured into three main stages: test, build, and deploy.

The pipeline implementation is available [here](https://gitlab.com/DolMalin/python-demoapp/-/blob/master/.gitlab-ci.yml).

### Test Stage 🧪

- Create Docker Images and install dependencies for unit test and linting.
- Invoke `pytest` and execute all unit tests written in the `src/app/test` directory.
- Invoke `flake8` and execute linting tests.
- Ensure that all the tests pass.

### Build Stage 🛠️

- Create `Flask` (app) and `Nginx` (reverse proxy) images.
- A Docker-in-Docker service is set up in both images, allowing `docker` commands to be executed inside the containers.
- Log in to the Docker registry (`hub.docker.com`).
- Push the built images to the registry.

### Deploy Stage 🚀
- Configure the SSH client and connect to an AWS EC2 instance.
- Log in to the Docker registry.
- Pull Docker images from the Docker registry.
- Create a Docker network to facilitate communication between the `Flask` and `Nginx` containers.
- Run the containers with docker compose.
